.. title: The Inari Project

Join our Fediverse services!

Active Services
###############

* `Kitsune Pod`_\ :sup:`*`, a `diaspora*`_ based social network
* `Nine Tails`_\ :sup:`#`,  a Friendica_ based social network

.. _Kitsune Pod:    https://kitsune.click/
.. _Nine Tails:     https://ninetails.click/
.. _diaspora*:      https://diasporafoundation.org/
.. _Friendica:      https://friendi.ca/

:sup:`*` Invites only. You can ask for an invite from any user or the admin, see contacts below

:sup:`#` Registration is closed

Contacts
########

======== ===================
 Admin:   admin@inari.click
 Abuse:   abuse@inari.click
======== ===================
