---
layout: default
sidebar_link: true
sidebar_sort_order: -1000
title: News
---

Follow our news accounts on our services:

* [inari@ninetails.click](https://ninetails.click/profile/inari)  - our main account
* [inari@kitsune.click](https://kitsune.click/u/inari) - reposts on Kitsune Pod
