---
layout: page
sidebar_link: true
sidebar_sort_order: 0
title: Privacy Policy
---

(basicially, copied from the default Friendica PP)

At the time of registration, and for providing communications between the user account and their contacts, the user has to provide a display name (pen name), an username (nickname) and a working email address. The names will be accessible on the profile page of the account by any visitor of the page, even if other profile details are not displayed. The email address will only be used to send the user notifications about interactions, but wont be visibly displayed. The listing of an account in the node's user directory or the global user directory is optional and can be controlled in the user settings, it is not necessary for communication.

This data is required for communication and is passed on to the nodes of the communication partners and is stored there. Users can enter additional private data that may be transmitted to the communication partners accounts.

At any point in time a logged in user can export their account data from the account settings. If the user wants to delete their account they find instructions at [Account Removal page](privacy/account_removal.html). The deletion of the account will be permanent. Deletion of the data will also be requested from the nodes of the communication partners.