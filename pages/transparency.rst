.. title: Transparency

This page is dedicated to transparency in applying our policies

Instance bans
#############

+--------------+-----------------+-----------------+-----------+
|              | Kitsune Pod     | Nine Tails      | reason    |
+--------------+-----------------+-----------------+-----------+
| \-           | \-              | \-              | \-        |
+--------------+-----------------+-----------------+-----------+

* **NSFW Media mark**: All media from this instance is automatically marked as NSFW
  (Pleroma's feature)
* **Silence** or **Shadow ban**: Hides the instance users from anyone who 
  isn't following them

Government-related actions
##########################

We do not have a canary page for now but all government actions will be posted here 
and we did not receive any secret subpoena as of now.
