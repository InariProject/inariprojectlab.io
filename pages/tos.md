---
layout: page
sidebar_link: true
sidebar_sort_order: 0
title: Terms of Service
---

These Terms of Service are valid for all Inari nodes.

We strongly believe in the freedom of speech so in general we impose very few rules.

# Rules for Humans

These rules apply to humans, other sentient beings and self-aware AIs

## Strictly forbidden activities

These lead to the immediate and eternal ban.

* Spam
* Strictly illegal activities like child pornography, promotion of terrorism and so on

## Forbidden activities

These may cause you permanent ban after investigation.

* Repeating or collective harassment of other users,
  including "dogpiling" and mention spamming
* Calls for violence against any individual or group

Please note that "we are oppressed minority so we can harass/oppress anyone"
will not work here.

## Things to avoid

Please also try to keep a friendly atmosphere and avoid things like

* NSFW content without marking
* Breaking rules of other nodes when posting comments there
* Doing illegal stuff by Canadian law

These things will be reviewed on a case to case basis and may lead to temporary or permanent ban.
Permanent ban is guaranteed if the account was created solely for these purposes.

## Applicability

No action performed outside of our nodes can be a reason for ban.

# Rules for Robots

Robot activity is strictly limited. Every bot has to be explicitly allowed by the
administration. Please contact admin@inari.click if you want to host a bot on one of 
our instances.

Also, bots should follow all the rules for humans, obviously.

## Explicitly forbidden bots

The list is not complete and may be extended in the future

* Bots that actively subscribe to other accounts (like federation bots)
* News aggregators
