.. title: Account Removal Links

+--------------+-----------------------------------------------------------+
| Nine Tails:  | https://ninetails.click/removeme                          |
+--------------+-----------------------------------------------------------+
| Kon Kon:     | | https://konkon.click/user-settings                      |
|              | | then scroll to the bottom to the Delete Account section |
+--------------+-----------------------------------------------------------+
| Inari Pics:  | please contact the administrator at admin@inari.click     |
+--------------+-----------------------------------------------------------+
| Inari Toot:  | | https://toot.inari.click/auth/edit                      |
|              | | then scroll to the bottom to the Delete Account section |
+--------------+-----------------------------------------------------------+
| Kitsune Pod: | | https://kitsune.click/user/edit                         |
|              | | then scroll to the bottom to the Close account section  |
+--------------+-----------------------------------------------------------+
